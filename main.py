from random import choice
import os


DRAWING = [
    "\n       \n        \n        \n        \n        \n        \n        ",
    "\n       \n        \n        \n        \n        \n        \n|     | ",
    "\n       \n        \n        \n        \n        \n_______ \n|     | ",
    "\n       \n|       \n|       \n|       \n|       \n|______ \n|     | ",
    "\n____   \n|       \n|       \n|       \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|       \n|       \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|       \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|   x   \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|  'x   \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|  'x'  \n|       \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|  'x'  \n|  '    \n|______ \n|     | ",
    "\n____   \n|   |   \n|   o   \n|  'x'  \n|  ' '  \n|______ \n|     | ",
    ]


def load_words():
    # Load popular english words
    word_file = open("data/popular.txt", "r")
    words = []
    for line in word_file:
        words.append(line.split()[0])
    word_file.close()
    return words


class HangmanGame:
    def __init__(self, words):
        self.correct_word = choice(words)
        self.guessed = self.correct_word[0]
        for i in range(1, len(self.correct_word)):
            self.guessed += "_"
        self.won = False
        self.attempts = 0
        self.drawing = DRAWING
        self.max_attempts = len(self.drawing) - 1
        print("")
        self.print_guess(self.guessed)
        self.start()

    def print_guess(self, override=False) -> None:
        os.system('cls' if os.name == 'nt' else 'clear')

        if not override:
            to_print = self.guessed
        else:
            to_print = override
        for letter in to_print:
            if letter == "_":
                print("_ ", end="")
            else:
                print(f"{letter.upper()} ", end="")
        print("\n")

    def update(self):
        new_guess = str(input("\nPlease input a letter: ")).upper()
        while len(new_guess) > 1:
            new_guess = str(input("Please input only one letter: ")).upper()
        self.guessed_copy = ""
        correct = False
        for i, letter in enumerate(self.correct_word):
            if self.guessed[i] != "_":
                self.guessed_copy += self.guessed[i]
            elif new_guess.lower() == letter.lower():
                self.guessed_copy += letter
                correct = True
            else:
                self.guessed_copy += "_"
        self.guessed = self.guessed_copy
        self.print_guess(self.guessed)
        self.won = (self.guessed.lower() == self.correct_word.lower())
        return correct

    def start(self):
        while not self.won:
            correct = self.update()
            if not correct:
                self.attempts += 1
            print(self.drawing[self.attempts])
            if self.attempts >= self.max_attempts:
                break
            print("###############")
        if self.won:
            print("Congratulations, you won! Attempts left:",
                  self.max_attempts - self.attempts,
                  )
        else:
            print("You ran out of attempts, you have been hanged.")
            print("The word was:", end="")
            self.print_guess(self.correct_word)


if __name__ == "__main__":
    words = load_words()
    keep_playing = True
    while keep_playing:
        HangmanGame(words)
        if not input("Play again ? (y/n) ").lower() in ["yes", "y"]:
            keep_playing = False
    print("\nThank you for playing")
